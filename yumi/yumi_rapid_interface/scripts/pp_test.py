#!/usr/bin/python3

import rospy
from geometry_msgs.msg import Point
from yumi_rapid_interface.srv import PickPlaceRAPID

def pick_place(pick_, place_, speed_, arm_):
    """
    
    """
    rospy.wait_for_service('/pick_place_rapid')
    call_rapid = rospy.ServiceProxy('/pick_place_rapid', PickPlaceRAPID)
    call_rapid(pick=pick_, place=place_, speed=speed_, arm=arm_)

if __name__ == "__main__":
    pick = Point(4, 1, 0)
    place = Point(2, 10, 4)
    arm = False
    speed = 170
    pick_place(pick, place, speed, arm)