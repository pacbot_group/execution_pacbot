#!/usr/bin/python3

from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup

# fetch values from package.xml
setup_args = generate_distutils_setup(
    scripts=[ 'src/yumi_rapid_interface/yumi_pp_rapid.py'],
    packages=['yumi_rapid_interface'],
    package_dir={'': 'src'},
    requires=['rospy', 'std_msgs', 'geometry_msgs', 'rospy_message_converter', 'abb_rapid_msgs', 'abb_robot_msgs',
    'abb_rapid_sm_addin_msgs', 'message_generation', 'message_runtime', 'abb_robot_driver']
)

setup(**setup_args)
